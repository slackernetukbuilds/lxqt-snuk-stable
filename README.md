# This project based on [LXQt](https://github.com/lxqt) &  [Slackware](http://www.slackware.com/) 

Unfortunately, Slackware 15.0 KDE packages are not compatible with LXQT 1.4.0.

I recommend to install Slackware 15.0 without KDE series, compatable KDE packages are part of this installation.
If you already have installed KDE, you should remove the uncompatible KDE packages.

`$ sudo slackpkg remove kde`


# Installing


**Building Instructions**

**_Slackware 15.0_**


```
$ git clone https://gitlab.com/slackernetukbuilds/lxqt-snuk-stable.git 
$ cd lxqt-snuk-stable 
$ sudo sh build.sh
```



**Configuration**

Add to /etc/slackpkg/blacklist file

`[0-9]+_snuk`

System-wide LXQt configuration files are in /usr/share/lxqt, while user configuration files are in ~/.config/lxqt.
For a standard setup, I recommend to copy the system-wide LXQt configuration files to ~/.config/lxqt

`cp -a /usr/share/lxqt ~/.config/`


`xwmconfig` to setup lxqt as standard-session



**ScreenShot**

![lxqt-1.4](/uploads/87c91c9bbe094cf9a1662be6ca183380/lxqt-1.4.png)


