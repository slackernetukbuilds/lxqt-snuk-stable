if [ -e usr/share/icons/Breeze_Blue/icon-theme.cache ]; then
  if [ -x /usr/bin/gtk-update-icon-cache ]; then
    /usr/bin/gtk-update-icon-cache -f usr/share/icons/Breeze_Blue >/dev/null 2>&1
  fi
fi
